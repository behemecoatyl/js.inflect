describe("Inflector", function() {

    var SingularToPlural = [
        ["search", "searches"],["switch", "switches"],["fix" , "fixes"],["box" , "boxes"],["process" , "processes"],
        ["address" , "addresses"],["case" , "cases"],["stack" , "stacks"],["wish" , "wishes"],["fish" , "fish"],
        ["jeans" , "jeans"],["funky jeans" , "funky jeans"],["my money" , "my money"],

        ["category" , "categories"],["query" , "queries"],["ability" , "abilities"],["agency" , "agencies"],["movie" , "movies"],

        ["archive" , "archives"],

        ["index" , "indices"],

        ["wife" , "wives"],["safe" , "saves"],["half" , "halves"],

        ["move" , "moves"],

        ["salesperson" , "salespeople"],["person" , "people"],

        ["spokesman" , "spokesmen"],["man" , "men"],["woman" , "women"],

        ["basis" , "bases"],["diagnosis" , "diagnoses"],["diagnosis_a" , "diagnosis_as"],

        ["datum" , "data"],["medium" , "media"],["stadium" , "stadia"],["analysis" , "analyses"],["my_analysis" , "my_analyses"],

        ["node_child" , "node_children"],["child" , "children"],

        ["experience" , "experiences"],["day" , "days"],

        ["comment" , "comments"],["foobar" , "foobars"],["newsletter" , "newsletters"],

        ["old_news" , "old_news"],["news" , "news"],

        ["series" , "series"],["species" , "species"],

        ["quiz" , "quizzes"],

        ["perspective" , "perspectives"],

        ["ox" , "oxen"],["photo" , "photos"],["buffalo" , "buffaloes"],["tomato" , "tomatoes"],["dwarf" , "dwarves"],
        ["elf" , "elves"],["information" , "information"],["equipment" , "equipment"],["bus" , "buses"],
        ["status" , "statuses"],["status_code" , "status_codes"],["mouse" , "mice"],

        ["louse" , "lice"],["house" , "houses"],["octopus" , "octopi"],["virus" , "viri"],["alias" , "aliases"],
        ["portfolio" , "portfolios"],

        ["vertex" , "vertices"],["matrix" , "matrices"],["matrix_fu" , "matrix_fus"],

        ["axis" , "axes"],["taxi" , "taxis"],["testis" , "testes"],["crisis" , "crises"],

        ["rice" , "rice"],["shoe" , "shoes"],

        ["horse" , "horses"],["prize" , "prizes"],["edge" , "edges"],

        ["cow" , "kine"],["database" , "databases"],["money drop" , "money drop"],['moneyguy', 'moneyguys'],

    // regression tests against improper inflection regexes
        ["|ice" , "|ices"],["|ouse" , "|ouses"],["slice" , "slices"],["police" , "police"]
    ];
    var ordinal = [
        [1,"1st"],[2,"2nd"],[3,"3rd"],[4,"4th"],[5,"5th"],[6,"6th"],[7,"7th"],[8,"8th"],[9,"9th"],
        [10,"10th"],[11,"11th"],[12,"12th"],[13,"13th"],[14,"14th"],
        [20,"20th"],[21,"21st"],[22,"22nd"],[23,"23rd"],[24,"24th"],
        [100,"100th"],[101,"101st"],[102,"102nd"],[103,"103rd"],[104,"104th"],
        [110,"110th"],[111,"111th"],[112,"112th"],[113,"113th"],
        [1000,"1000th"],[1001, "1001st"]
    ];
    ordinal.zero = [0,"0th"];

    it('should pluralize correctly', function() {
        _.each(SingularToPlural, function(item) {
            expect(_.inflector.pluralize(item[0])).toBe(item[1]);
        });
    });

    it('should ordinalize correctly', function() {
        expect(_.inflector.ordinalize(ordinal.zero[0])).toBe(ordinal.zero[1]);
        expect(_.inflector.ordinalize(-ordinal.zero[0])).toBe(ordinal.zero[1]);
        _.each(ordinal, function(item) {
            expect(_.inflector.ordinalize(item[0])).toBe(item[1]);
            expect(_.inflector.ordinalize(-item[0])).toBe('-'+item[1]);
        });
    });

    it('have the right locale', function() {
        expect(_.inflector.locale).toBe('en');
        _.inflector.locale = 'fr';
        expect(_.inflector.locale).toBe('fr');
    })


});

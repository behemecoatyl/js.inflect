"use strict";
(function(exports) {

    var rules = {}, locale = null,
        setup = {};

    // private stuff
    function clean_str(str, lower) {
        lower = lower || true;
        return lower ? str.trim().toLowerCase() : str.trim();
    }

    function rule_for(pattern, replacement) {
        pattern = typeof pattern === 'string' ? new RegExp(pattern.toString(), 'i') : pattern;
        replacement = replacement.toString();
        return {
            inspect: function() {console.log(pattern + ' - ' + replacement);},
            match: function(word) {
                return pattern.test(word);
            },
            replace: function(word) {
                return pattern.test(word) ? word.replace(pattern, replacement) : null;
            }
        };
    }

    function rule_set() {
        return {
            plurals: [], singulars: [], humans: [], // Array<Object{#match(word):bool; #replace(word):string?}>
            uncountables: [],                       // Array<String>
            acronyms: {}                            // Hash<String, String>
        };
    }

    function setup_locale(ln) {
        ln = ln || 'en';
        if (!rules[ln]) {
            rules[ln] = rule_set();
        }
        locale = ln;
    }

    function rm_uncountable() {
        var i, l;
        for (i = 0, l = arguments.length; i < l; i++) {
            if (typeof arguments[i] === 'string') {
                rules[locale].uncountables = _.without(rules[locale].uncountables, clean_str(arguments[i]));
            }
        }
    }

    // main interface
    function inflector() {
        inflector.locale = arguments[0];
        return inflector;
    }

    Object.defineProperties(inflector, {
        'locale': {
            get: function() { return locale; },
            set: function(ln) { setup_locale(ln); }
        }
    });

    // chainable methods to register inflexion rules
    setup.acronym = (function(word) {
        rules[locale].acronyms[clean_str(word)] = word;
        return this;
    }).bind(setup);
    setup.plural = (function(rule, replacement) {
        rm_uncountable(rule, replacement);
        rules[locale].plurals.unshift(rule_for(rule, replacement));
        return this;
    }).bind(setup);
    setup.singular = (function(rule, replacement) {
        rm_uncountable(rule, replacement);
        rules[locale].singulars.unshift(rule_for(rule, replacement));
        return this;
    }).bind(setup);
    setup.irregular = (function(singular, plural) {
        var s0 = singular.substr(0,1), srest = singular.substr(1),
            p0 = plural.substr(0,1), prest = plural.substr(1);
        if (s0.toUpperCase() === p0.toUpperCase()) {
            this.plural(singular+"\\b",  plural);
            this.plural(plural+"\\b",    plural);
            this.singular(singular+"\\b",singular);
            this.singular(plural+"\\b",  singular);
        } else {
            this.plural(new RegExp(s0.toUpperCase()+'(.?)'+srest+"$"), p0.toUpperCase() + prest);
            this.plural(new RegExp(s0.toLowerCase()+'(.?)'+srest+"$"), p0.toLowerCase() + prest);
            this.plural(new RegExp(p0.toUpperCase()+'(.?)'+prest+"$"), p0.toUpperCase() + prest);
            this.plural(new RegExp(p0.toLowerCase()+'(.?)'+prest+"$"), p0.toLowerCase() + prest);

            this.singular(new RegExp(s0.toUpperCase()+'(.?)'+srest+"$"), s0.toUpperCase() + srest);
            this.singular(new RegExp(s0.toLowerCase()+'(.?)'+srest+"$"), s0.toLowerCase() + srest);
            this.singular(new RegExp(p0.toUpperCase()+'(.?)'+prest+"$"), s0.toUpperCase() + srest);
            this.singular(new RegExp(p0.toLowerCase()+'(.?)'+prest+"$"), s0.toLowerCase() + srest);
        }
        return this;
    }).bind(setup);
    setup.uncountable = (function() {
        var i, l;
        for (i = 0, l = arguments.length; i < l; i++) {
            if (typeof arguments[i] === 'string') {
                rules[locale].uncountables.push(clean_str(arguments[i]));
            }
        }
        return this;
    }).bind(setup);
    setup.human = (function(rule, replacement) {
        rules[locale].humans.unshift(rule_for(rule, replacement));
        return this;
    }).bind(setup);
    setup.ordinalize_with = (function(f) {
        if (Object.prototype.toString.call(f) === "[object Function]") {
            rules[locale].ordanize = f;
        }
        return this;
    }).bind(setup);

    inflector.register = function() {
        var c = arguments[0] || null, k, r;
        if (_.isFunction(c)) {
            c.call(inflector, setup);
        } else {
            // todo implements object based
            if (typeof c === 'object') {

                k = _.keys(c);
                r = rule_set();
                _.each(_.keys(r), function(k) {

                });
            }
        }
    };

    // inflexion opertions
    function inflect(word, ruleset, ln) {
        var result;
        setup_locale(ln);

        if (word.length === 0) {

            return word;
        } else {
            result = _.find(rules[locale].uncountables, function(item) {
                return (new RegExp("\\b" +item+ "\\b", 'i')).test(word);
            });
            if (result) {
                return word.toString();
            }
            result = _.find(rules[locale][ruleset], function(rule) {
                //rule.inspect();
                return rule.match(word);
            });
            return result ? result.replace(word) : word.toString();
        }
    }

    inflector.pluralize = function() {
        return inflect(arguments[0], 'plurals', arguments[1] || locale);
    };
    inflector.singularize = function() {
        return inflect(arguments[0], 'singulars',  arguments[1] || locale);
    };
    inflector.dasherize = function(underscore_word) {
        return underscore_word.replace(/_+/, '-');
    };
    inflector.ordinalize = function(num, default_return) {
        if (Object.prototype.toString.call(rules[locale].ordanize) !== "[object Function]") {
            throw 'No ordinalizer registred for locale ' + locale + '.';
        }
        return isNaN(Number(num)) ? (default_return || num) : rules[locale].ordanize(Number(num));
    };
    inflector.capitalize = function(word) {
        return word.substr(0,1).toUpperCase() + word.substr(1).toLowerCase();
    };
    inflector.uncapitalize = function(word) {
        return word.substr(0,1).toLowerCase() + word.substr(1);
    };
    inflector.humanize = function(lowercaseAndUnderscoredWord) {
        return inflector.capitalize(lowercaseAndUnderscoredWord.replace('_', ' '));
    };

    // init default locale
    inflector('en').register(function(inflect) {
        inflect.plural(/$/, 's').plural(/s$/i, 's').plural(/^(ax|test)is$/i, "$1es").plural(/(octop|vir)us$/i, "$1i")
            .plural(/(octop|vir)i$/i, "$1i").plural(/(alias|status)$/i, "$1es").plural(/(bu)s$/i, "$1ses")
            .plural(/(buffal|tomat)o$/i, "$1oes").plural(/([ti])um$/i, "$1a").plural(/([ti])a$/i, "$1a").plural(/sis$/i, "ses")
            .plural(/(?:([^f])fe|([lr])f)$/i, "$1$2ves").plural(/(hive)$/i, "$1s").plural(/([^aeiouy]|qu)y$/i, "$1ies")
            .plural(/(x|ch|ss|sh)$/i, "$1es").plural(/(matr|vert|ind)(?:ix|ex)$/i, "$1ices").plural(/^(m|l)ouse$/i, "$1ice")
            .plural(/^(m|l)ice$/i, "$1ice").plural(/^(ox)$/i, "$1en").plural(/^(oxen)$/i, "$1").plural(/(quiz)$/i, "$1zes")

            .singular(/s$/i, '').singular(/(ss)$/i, "$1").singular(/(n)ews$/i, "$1ews").singular(/([ti])a$/i, "$1um")
            .singular(/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)(sis|ses)$/i, "$1sis").singular(/(^analy)(sis|ses)$/i, "$1sis")
            .singular(/([^f])ves$/i, "$1fe").singular(/(hive)s$/i, "$1").singular(/(tive)s$/i, "$1").singular(/([lr])ves$/i, "$1f")
            .singular(/([^aeiouy]|qu)ies$/i, "$1y").singular(/(s)eries$/i, "$1eries").singular(/(m)ovies$/i, "$1ovie")
            .singular(/(x|ch|ss|sh)es$/i, "$1").singular(/^(m|l)ice$/i, "$1ouse").singular(/(bus)(es)?$/i, "$1").singular(/(o)es$/i, "$1")
            .singular(/(shoe)s$/i, "$1").singular(/(cris|test)(is|es)$/i, "$1is").singular(/^(a)x[ie]s$/i, "$1xis")
            .singular(/(octop|vir)(us|i)$/i, "$1us").singular(/(alias|status)(es)?$/i, "$1").singular(/^(ox)en/i, "$1")
            .singular(/(vert|ind)ices$/i, "$1ex").singular(/(matr)ices$/i, "$1ix").singular(/(quiz)zes$/i, "$1").singular(/(database)s$/i, "$1")

            .irregular('person', 'people').irregular('man', 'men').irregular('child', 'children').irregular('sex', 'sexes')
            .irregular('move', 'moves').irregular('cow', 'kine').irregular('zombie', 'zombies')

            .uncountable('equipment','information','rice','money','species','series','fish','sheep','jeans','police');

        inflect.ordinalize_with(function(num) {
            var anum;
            if (num === 0) {
                return '0th';
            } else {
                anum = Math.abs(num);
                switch (anum % 10) {
                    case 1: return num + (anum % 100 === 11 ? 'th': 'st');
                    case 2: return num + (anum % 100 === 12 ? 'th': 'nd');
                    case 3: return num + (anum % 100 === 13 ? 'th': 'rd');
                    default: return num + "th";
                }
            }
        });
    });
    exports.inflector = inflector;

})(_);
